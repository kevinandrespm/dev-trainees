# EntregaPaises

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Publicar app en servidor de prueba (Pasos)

1) ng build
2) Verificar el archivo index.html generado y la base URL debe sere la ruta completa del server
3) alt+shift+F (Formatear documento)
4) Hacer los links con la URL del sever
5) Subir el contenido de la carpeta dist en la ruta /var/www/html/learning
6) Verificar que se despliegue la aplicación correctamente en http://144.217.242.247/learning/