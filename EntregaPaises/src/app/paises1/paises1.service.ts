import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs';
import { PaisModel } from './pais.model';

@Injectable({
  providedIn: 'root'
})
export class PaisesService {

  private serverUrl = 'http://144.217.242.247/WsBoater/';

  //Se añaden los routings
  SERVER_RECURSO_CARGAR = "Paises/listar";
  SERVER_RECURSO_ELIMINAR = "Paises/eliminar";
  SERVER_RECURSO_ACTUALIZAR = "Paises/actualizar";
  SERVER_RECURSO_CREAR = "Paises/guardar";


  constructor(private http: HttpClient) {
  }

  //Todos los métodos de servicios constan de la misma estructura
  cargarPaises(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    return this.http.get<PaisModel>(this.serverUrl + this.SERVER_RECURSO_CARGAR, { params });
  };

  
  eliminarPais(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    return this.http.get(this.serverUrl + this.SERVER_RECURSO_ELIMINAR, { params });
  };

  actualizarPais(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    return this.http.get<any>(this.serverUrl + this.SERVER_RECURSO_ACTUALIZAR, { params });
  };

  adicionarPais(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    return this.http.get<any>(this.serverUrl + this.SERVER_RECURSO_CREAR, { params });
  };

  getServerUrl():string {
    return this.serverUrl;
  }
  
}