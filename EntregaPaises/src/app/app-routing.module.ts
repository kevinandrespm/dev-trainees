import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Paises1Component } from './paises1/paises1.component';

import { MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Paises2Component } from './paises2/paises2.component';
import { IdiomasComponent } from './idiomas/idiomas.component';
import { WsUsuariosComponent } from './ws-usuarios/ws-usuarios.component';

const routes: Routes = [
  { path: '', component:Paises1Component  },
  { path: "test", component: Paises1Component },
  { path: "paises2", component: Paises2Component },
  {path: "idiomas", component: IdiomasComponent},
  {path: "usuarios", component: WsUsuariosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [
    RouterModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppRoutingModule { }
