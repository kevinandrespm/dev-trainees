import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Paises1Component } from './paises1/paises1.component';
import { Paises2Component } from './paises2/paises2.component'
import { RouterModule, Routes } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';

import { MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { from } from 'rxjs';
import { IdiomasComponent } from './idiomas/idiomas.component';
import { WsUsuariosComponent } from './ws-usuarios/ws-usuarios.component';


@NgModule({
  declarations: [
    AppComponent,
    Paises1Component,
    Paises2Component,
    IdiomasComponent,
    WsUsuariosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    RouterModule
  ],
  providers: [Paises1Component],
  bootstrap: [AppComponent],
  exports: [
    FormsModule,
    ReactiveFormsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
