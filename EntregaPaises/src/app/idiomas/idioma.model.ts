export class IdiomaModel{
    ID:string;
    NOMNRE_IDIOMA: string;
    MCA_ACTIVO: string;
    MCA_ELIMINADO: string;
    SHORT_NAME: string;
    BANDERA: string;
}   