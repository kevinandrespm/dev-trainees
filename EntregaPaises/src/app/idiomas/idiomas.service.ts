import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs';
import { IdiomaModel } from './idioma.model';

@Injectable({
  providedIn: 'root'
})
export class IdiomasService {

  private serverUrl = 'http://144.217.242.247/WsBoater/';

  //Se añaden los routings
  SERVER_RECURSO_CARGAR = "Idiomas/listar";
  SERVER_RECURSO_ELIMINAR = "Idiomas/eliminar";
  SERVER_RECURSO_ACTUALIZAR = "Idiomas/actualizar";
  SERVER_RECURSO_CREAR = "Idiomas/guardar";


  constructor(private http: HttpClient) {
  }

  //Todos los métodos de servicios constan de la misma estructura
  cargarIdiomas(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    return this.http.get<IdiomaModel>(this.serverUrl + this.SERVER_RECURSO_CARGAR, { params });
  };

  actualizarIdioma(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    return this.http.get<any>(this.serverUrl + this.SERVER_RECURSO_ACTUALIZAR, { params });
  };

  adicionarIdioma(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    return this.http.get<any>(this.serverUrl + this.SERVER_RECURSO_CREAR, { params });
  };

  getServerUrl():string {
    return this.serverUrl;
  }
  
}