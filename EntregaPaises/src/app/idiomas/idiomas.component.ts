import { Component, OnInit } from '@angular/core';
import { IdiomaModel } from './idioma.model';
import { IdiomasService } from './idiomas.service';
import { FormControl, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-idiomas',
  templateUrl: './idiomas.component.html',
  styleUrls: ['./idiomas.component.scss']
})
export class IdiomasComponent implements OnInit {

  pais:IdiomaModel = new IdiomaModel();
  public userFormIdioma: FormGroup;
  listadoIdiomas:any = {};
  public lShowBtnActualizar: Boolean = false;
  public lShowBtnAdicionar: Boolean = false;
  public lShowBtnEliminar: Boolean = false;
  public dialogRef : any;
  public lShowPanelDatosIdiomas: Boolean = false;
  public lShowPanelListadoIdiomas: Boolean = true;
  
  public idiomaEliminar:any;

  constructor(private _lService: IdiomasService) { }

  ngOnInit() {

    console.log("Init");
    this.loadListado(); 

    this.userFormIdioma = new FormGroup({
      
      ID: new FormControl('', [Validators.maxLength(60)]),
      NOMBRE_IDIOMA: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      SHORT_NAME: new FormControl('', [Validators.required, Validators.maxLength(50)]), 
      MCA_ACTIVO: new FormControl('', [Validators.required, Validators.maxLength(50)]), 
      MCA_ELIMINADO: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      BANDERA: new FormControl('', [Validators.required, Validators.maxLength(50)])
    }); 
  }

  loadListado() {
    //console.log("SE RECIBEE: ", obj)
    this._lService.cargarIdiomas().subscribe(data => {
      console.log('cargar data', data);
      this.listadoIdiomas = data;
    });
       
    this.lShowPanelListadoIdiomas = true;
    this.lShowBtnActualizar = false;
    this.lShowBtnEliminar = false;
    this.lShowBtnAdicionar = false;
    this.lShowPanelDatosIdiomas = false;
  }

  adicionar() {
    this.userFormIdioma.reset();
    this.lShowBtnAdicionar = true;
    this.lShowBtnActualizar = false;
    this.lShowBtnEliminar = false;

    this.lShowPanelDatosIdiomas = true;
    this.lShowPanelListadoIdiomas = false;
  }

  actualizar(user) {
    this.userFormIdioma.patchValue(user);
    console.log(this.userFormIdioma.value);
    this.lShowBtnActualizar = true;
    this.lShowBtnEliminar = true;
    this.lShowBtnAdicionar = false;
    this.lShowPanelDatosIdiomas = true;
    this.lShowPanelListadoIdiomas = false;
  }

  callCancelar() {    
    this.showListado();
    this.userFormIdioma.reset();
  }

  showListado() {
    this.lShowBtnActualizar = false;
    this.lShowBtnEliminar =false;
    this.lShowBtnAdicionar = false;
    this.lShowPanelDatosIdiomas = false;
    this.lShowPanelListadoIdiomas = true;
  }

  callUpdate() {
    console.log(this.userFormIdioma.value);
    if (!this.userFormIdioma.valid){
      return true;
    }
    this._lService.actualizarIdioma(this.userFormIdioma.value).subscribe(data => {
      console.log(data);
      if (data.mensaje == "TRUE") {
        this.loadListado();
      }
    });
  }

  callAdd() {
    console.log(this.userFormIdioma.value);
    this._lService.adicionarIdioma(this.userFormIdioma.value).subscribe(data => {
      if (data.mensaje == "TRUE") {       
        this.loadListado();
      }
    });
  }
}
