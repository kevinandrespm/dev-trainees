import { Component, OnInit } from '@angular/core';
import { Paises2Service } from 'src/app/paises2/paises2.service';
import { PaisModel } from './pais.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-paises2',
  templateUrl: './paises2.component.html',
  styleUrls: ['./paises2.component.scss']
})
export class Paises2Component implements OnInit {

  pais:PaisModel = new PaisModel();
  public userForm: FormGroup;
  listadoPaises:any = {};
  public lShowBtnActualizar: Boolean = false;
  public lShowBtnAdicionar: Boolean = false;
  public lShowBtnEliminar: Boolean = false;
  public dialogRef : any;
  public lShowPanelDatosPaises: Boolean = false;
  public lShowPanelListadoPaises: Boolean = true;

  public paisEliminar:any;

  constructor(private _servicio: Paises2Service) { }

  ngOnInit() {
    console.log("entra ngOnInit");
    this.loadListado();  
    
    this.userForm = new FormGroup({
      
      codigoInternacional: new FormControl('', [Validators.maxLength(60)]),
      id: new FormControl('', [Validators.maxLength(60)]),
      nombre: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      bandera: new FormControl('', [Validators.required, Validators.maxLength(50)]),            
    }); 
  }

  loadListado(obj?) {
    this._servicio.listarPaises(obj).subscribe(data => {
      console.log('cargar data', data)
      this.listadoPaises = data;
    });    
    this.lShowPanelListadoPaises = true;
    this.lShowBtnActualizar = false;
    this.lShowBtnEliminar = false;
    this.lShowBtnAdicionar = false;
    this.lShowPanelDatosPaises = false;
  }

  // Metodos crud, solo sirven para mostrar los datos en pantalla
  //e inicalizar variables y valores y llenar inputs.. FORM.
  
  adicionar() {
    this.userForm.reset();
    this.lShowBtnAdicionar = true;
    this.lShowBtnActualizar = false;
    this.lShowBtnEliminar = false;

    this.lShowPanelDatosPaises = true;
    this.lShowPanelListadoPaises = false;
  }

  actualizar(user) {
    console.log("entramos en actualizar");
    this.userForm.patchValue(user);
    console.log(this.userForm.value);
    this.lShowBtnActualizar = true;
    this.lShowBtnEliminar = true;
    this.lShowBtnAdicionar = false;
    this.lShowPanelDatosPaises = true;
    this.lShowPanelListadoPaises = false;
  }

  callCancelar() {    
    this.showListado();
    this.userForm.reset();
  }

  showListado() {
    this.lShowBtnActualizar = false;
    this.lShowBtnEliminar =false;
    this.lShowBtnAdicionar = false;
    this.lShowPanelDatosPaises = false;
    this.lShowPanelListadoPaises = true;
  }

  //Metodos Call, Ejecutan el llamado del servicio.
  
  callUpdate() {
    console.log(this.userForm.value);
    if (!this.userForm.valid){
      return true;
    }
    this._servicio.actualizarPais(this.userForm.value).subscribe(data => {
      console.log(data);
      if (data.mensaje == "TRUE") {
        this.loadListado();
      }
    });
  }

  callAdd() {
    console.log(this.userForm.value);
    this._servicio.adicionarPais(this.userForm.value).subscribe(data => {
      if (data.mensaje == "TRUE") {       
        this.loadListado();
      }
    });
  }

  callEliminar(user) {
    console.log("callEliminar");
    this.paisEliminar = user;
    this._servicio.eliminarPais(this.paisEliminar).subscribe(data => {
      console.log('eliminar data', data)
      if (data.mensaje == "TRUE") {
        this.loadListado();
      }
    });
  }
}
