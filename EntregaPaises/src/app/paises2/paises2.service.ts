import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class Paises2Service {
  serverUrl:string;

  listar = "Paises/listar";
  eliminar = "Paises/eliminar";
  actualizar = "Paises/actualizar";
  guardar = "Paises/guardar";

  constructor(private httpClient:HttpClient) {
    this.serverUrl = 'http://144.217.242.247/WsBoater/';
  }

  listarPaises(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    console.log(params);
    return this.httpClient.get<any>(this.serverUrl + this.listar, { params });
  };

  eliminarPais(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    return this.httpClient.get<any>(this.serverUrl + this.eliminar, { params });
  };
  actualizarPais(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    return this.httpClient.get<any>(this.serverUrl + this.actualizar, { params });
  };

  adicionarPais(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    return this.httpClient.get<any>(this.serverUrl + this.guardar, { params });
  };
  getServerUrl():string {
    return this.serverUrl;
  }
}