import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WsUsuariosService {

  serverUrl:string;

  listar = "WsUsers/listarUsuarios";
  eliminar = "WsUsers/eliminar";
  actualizar = "WsUsers/actualizar";
  guardar = "WsUsers/guardarusuario";

  constructor(private httpClient:HttpClient) {
    this.serverUrl = 'http://144.217.242.247/WsBoater/';
  }

  listarUsuarios(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    console.log(params);
    return this.httpClient.get<any>(this.serverUrl + this.listar, { params });
  };

  eliminarUsuarios(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    return this.httpClient.get<any>(this.serverUrl + this.eliminar, { params });
  };
  actualizarUsuarios(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    return this.httpClient.get<any>(this.serverUrl + this.actualizar , { params });
  };

  adicionarUsuarios(objeTosend?): Observable<any> {
    const params = new HttpParams({
      fromObject: objeTosend
    });
    return this.httpClient.get<any>(this.serverUrl +this.guardar , { params });
  };
  getServerUrl():string {
    return this.serverUrl;
  }
}
