import { Component, OnInit } from '@angular/core';
import { WsUsuariosService } from '../ws-usuarios/ws-usuarios.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-ws-usuarios',
  templateUrl: './ws-usuarios.component.html',
  styleUrls: ['./ws-usuarios.component.scss']
})
export class WsUsuariosComponent implements OnInit {

  public userForm: FormGroup;
  listadoUsuarios:any = {};
  public lShowBtnActualizar: Boolean = false;
  public lShowBtnAdicionar: Boolean = false;
  public lShowBtnEliminar: Boolean = false;
  public dialogRef : any;
  public lShowPanelDatosPaises: Boolean = false;
  public lShowPanelListadoPaises: Boolean = true;

  public usuarioEliminar:any;

  constructor(private http: WsUsuariosService) { }

  ngOnInit() {
    console.log("entra ngOnInit");
    this.loadListado();  
    
    this.userForm = new FormGroup({
     
      ID: new FormControl('', [Validators.maxLength(60)]),
      USUARIO_TELEFONO: new FormControl('', [Validators.maxLength(60)]),
      USUARIO_NOMBRE: new FormControl('', [Validators.maxLength(60)]),
      USUARIO_APELLIDO: new FormControl('', [Validators.maxLength(60)]),
      USUARIO_LOGIN: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      USUARIO_PASSWORD: new FormControl('', [Validators.required, Validators.maxLength(50)]),            
      EMPRESA_ID: new FormControl('', [Validators.required, Validators.maxLength(50)]),            
      USUARIO_CORREO: new FormControl('', [Validators.required, Validators.maxLength(50)]),            
      CIUDAD_ID: new FormControl('', [Validators.required, Validators.maxLength(50)]),            
      PUNTO_ID: new FormControl('', [Validators.required, Validators.maxLength(50)]),            
      MCA_EMPLEADO: new FormControl('', [Validators.required, Validators.maxLength(50)]),            
      IDIOMA_ID: new FormControl('', [Validators.required, Validators.maxLength(50)]),          
    }); 
  }

  loadListado(obj?) {
    this.http.listarUsuarios(obj).subscribe(data => {
      console.log('cargar data', data)
      this.listadoUsuarios = data;
    });    
    this.lShowPanelListadoPaises = true;
    this.lShowBtnActualizar = false;
    this.lShowBtnEliminar = false;
    this.lShowBtnAdicionar = false;
    this.lShowPanelDatosPaises = false;
  }

  // Metodos crud, solo sirven para mostrar los datos en pantalla
  //e inicalizar variables y valores y llenar inputs.. FORM.
  
  adicionar() {
    this.userForm.reset();
    this.lShowBtnAdicionar = true;
    this.lShowBtnActualizar = false;
    this.lShowBtnEliminar = false;
    this.lShowPanelDatosPaises = true;
    this.lShowPanelListadoPaises = false;
  }

  actualizar(user) {
    console.log("entramos en actualizar");
    this.userForm.patchValue(user);
    console.log(this.userForm.value);
    this.lShowBtnActualizar = true;
    this.lShowBtnEliminar = true;
    this.lShowBtnAdicionar = false;
    this.lShowPanelDatosPaises = true;
    this.lShowPanelListadoPaises = false;
  }

  callCancelar() {    
    this.showListado();
    this.userForm.reset();
  }

  showListado() {
    this.lShowBtnActualizar = false;
    this.lShowBtnEliminar =false;
    this.lShowBtnAdicionar = false;
    this.lShowPanelDatosPaises = false;
    this.lShowPanelListadoPaises = true;
  }

  //Metodos Call, Ejecutan el llamado del servicio.
  
  callUpdate() {
    console.log(this.userForm.value);
    if (!this.userForm.valid){
      return true;
    }
    this.http.actualizarUsuarios(this.userForm.value).subscribe(data => {
      console.log(data);
      if (data.mensaje == "TRUE") {
        this.loadListado();
      }
    });
  }

  callAdd() {
    console.log(this.userForm.value);
    this.http.adicionarUsuarios(this.userForm.value).subscribe(data => {
      if (data.mensaje == "TRUE") {       
        this.loadListado();
      }
    });
  }

  callEliminar(user) {
    console.log("callEliminar");
    this.usuarioEliminar = user;
    this.http.eliminarUsuarios(this.usuarioEliminar).subscribe(data => {
      console.log('eliminar data', data)
      if (data.mensaje == "TRUE") {
        this.loadListado();
      }
    });
  }

}
